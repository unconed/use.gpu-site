import { readFileSync } from 'fs';
import { Document, PackageFile } from '../types';

const extractTSDocAnnotations = (s?: string) => {
  if (s == null) return { text: '', props: {} };
  
  const lines = s.split("\n");
  const props: Record<string, any> = {};

  const out: string[] = [];
  for (const line of lines) {
    const m = line.match(/(^|(?<=\s))@[a-z]+/);
    if (m) {
      if (m[0] == '@hidden') props.hidden = true;
    }
    else out.push(line);
  }
  return {
    text: out.join("\n"),
    props,
  };
};

export const packageTsToDocs = (pkg: PackageFile, root: Document, docs: Record<string, Record<string, any>>) => {
  
  const {meta: {name}} = pkg;
  const docRefs = docs[name];
  if (!docRefs) return [];

  const allRefs: Record<string, [Record<string, any>, string]> = {};
  for (let pkg in docs) {
    const pkgRefs = docs[pkg];
    for (let k in pkgRefs) {
      allRefs[k] = [pkgRefs[k], pkg];
    }
  }

  const out: Document[] = [];
  
  for (const k in docRefs) {
    const ref = docRefs[k];
    const {id} = ref;
    
    const slug = root.slug + (id.match(/^[a-z]/) ? '--' : '-') + id;
    const base = root.base + id + '/';

    const {text, props} = extractTSDocAnnotations(ref.description);
    ref.description = text;
    
    const title = id;
    const {content, icon, score} = formatDocRef(ref, pkg, allRefs);

    if (props.hidden) continue;

    if (id == null) {
      console.log('!!! null', {ref});
      continue;
    }
    if (id.match(/Props$/)) {
      const componentRef = allRefs[id.replace(/Props$/, '')];
      if (componentRef && formatDocRef(componentRef[0], pkg, allRefs).icon === 'widgets') continue;
    }

    out.push({
      title,
      content,
      href: `/docs/${slug}`,
      icon,
      slug,
      base,

      namespace: name.split('/')[1],
      order: -score,
      score,
    })
  }
  
  return out;
};

const H2 = (t: string) => ['## ' + t];
const H3 = (t: string) => ['### ' + t];
const H4 = (t: string) => ['#### ' + t];
const B = (t: string) => ['**' + t + '**'];
const Code = (s: string[]) =>
  [
    '```tsx',
    formatCode(s),
    '```',
  ];

const formatCode = (s: string[]) => {
  const code = s.join("\n");
  return indent(
    code
    .replace(/^\n+|\n+$/, '')
    .replace(/\n{\n/g, ' {\n')
    .replace(/: +\| /g, ': ')
    .replace(/\n\n/g, '\n')
    .replace(/\( +/g, '(')
    .replace(/, *\)/g, ')')
    .replace(/, *([}\]])/g, ' $1')
    .replace(/\(\|/g, '(')
    .split("\n")
  ).join("\n")
  .replace(/  \) => {/g, ') => {');
}

const COMPONENT_TYPES = new Set(['LiveComponent', 'FunctionComponent', 'LC', 'FC']);
const CONTEXT_TYPES = new Set(['LiveContext']);

const formatDocRef = (docRef: any, pkg: any, allRefs: any): {
  content: string,
  icon: string,
  score: number,
} => {
  const out: string[] = [];

  let icon = 'description';
  let score = 0; 

  const {id, kind, type, description, loc} = docRef;

  const Header = (t: string, i: string, s: number, d: string = id) => {
    const suffix = `<span style="float: right" className="m-icon m-icon-large" title="${t}">${i}</span>`;
    out.push(`# ${suffix} ${t} – ${d}`);
    
    icon = i;
    score = s;
  };
  
  if (kind === 'typealias') {
    Header("Type", 'title', 2);
    out.push(...Code(formatTypeAlias(docRef)));
    out.push(description);
  }
  else if (kind === 'variable' && id.match(/^(use[A-Z]|makeUse[A-Z])/)) {
    Header("Hook", 'phishing', 6);
    out.push(...Code(formatFunction(docRef)));
    out.push(description);
  }
  else if (kind === 'variable' && type == 'Function') {
    Header("Function", 'east', 5);
    out.push(...Code(formatFunction(docRef)));
    out.push(description);
  }
  else if (kind === 'variable' && id.match(/^([A-Z][a-z]*)+Context|Consumer$/)) {
    Header("Context", 'input', 4);
    out.push(...Code(formatConstant(docRef)));
    out.push(description);
    
    if (CONTEXT_TYPES.has(docRef.type)) {
      let propRef = docRef.typeArgs?.[0];
      if (propRef) {
        out.push(...H2("Props"));
        out.push(...formatComponentProps(propRef, allRefs));
      }
    }
  }
  else if (kind === 'variable') {
    if (COMPONENT_TYPES.has(docRef.type)) {
      Header("Component", 'widgets', 7, '&lt;' + id + '>');
      out.push(...Code(formatConstant(docRef)));
      out.push(description);

      let propRef = docRef.typeArgs?.[0];
      if (propRef) {
        out.push(...H2("Props"));
        out.push(...formatComponentProps(propRef, allRefs));
      }
    }
    else {
      const typeRef = allRefs[docRef.type];
      const value = typeRef?.[0];

      if (value?.signatures) {
        Header("Function", 'east', 5);
      } else {
        Header("Constant", 'data_object', 3);
      }
      out.push(...Code(formatConstant(docRef, value)));
      out.push(description);
    }
  }
  else if (kind === 'enum') {
    Header('Enum', 'format_list_bulleted', 1);
    out.push(...Code(formatConstant(docRef)));
    out.push(description);
  }
  else if (kind === 'reexport') {
    Header('Export', 'highlight_alt', 1);
    out.push(description);
  }
  else if (kind === 'interface') {
    Header('Interface', 'title', 2);
    out.push(...Code(formatType(docRef)));
    out.push(description);
  }
  else {
    console.log('!!! unstyled docRef', {docRef})
    out.push(`# ${docRef.id}`);
  }
  
  const {file, line} = loc;
  const ns = pkg.meta.name.split('/')[1];
  out.push(`## Source`);

  const url = "https://gitlab.com/unconed/use.gpu/-/blob/master/packages/" + ns + "/" + file.replace(/^\./, '') + "#L" + line;
  out.push(`packages <span className="muted">/</span> ${ns} <span className="muted">/</span> <a href="${url}">${file.replace(/\//g, ' <span className="muted">/</span> ')}</a>`);

  return {
    content: out.join("\n"),
    icon,
    score,
  };
};

const formatComponentProps = (docRef: any, allRefs: any, optional: boolean = false): string[] => {

  if (docRef.type === 'intersection' || docRef.type === 'union') {
    const {typeArgs} = docRef;

    let compatible = true;

    const chunks: [string, string[]][] = [];

    let i = 0;
    for (let type of typeArgs) {
      const out: string[] = [];

      let opt = optional;
      if (type.type === 'Partial') {
        opt = true;
        type = type.typeArgs?.[0];
      }

      let t = formatType(type).join(' ');

      const typeRef = allRefs[t];
      if (typeRef) {
        out.push(...H3(t));
        out.push(...formatComponentProps(typeRef[0], allRefs, opt));
        chunks.push([t, out]);
      }
      else {
        const t = typeArgs[i];
        if (t.type === 'Object') {
          out.push(...H3("Other"));
          out.push(...formatComponentProps(t, allRefs, opt));
          chunks.push(['zzzzzz', out]);
        }
        else if (t.type === 'null' || t.type === 'undefined') {
        }
        else {
          compatible = false;
          break;
        }
      }
      ++i;
    }
    
    if (compatible) {
      chunks.sort((a, b) => a[0].localeCompare(b[0]));
      return chunks.flatMap(([, text]) => text);
    }
  }

  if (docRef.type === 'Object') {
    const {properties} = docRef;
    const out: string[] = [];
    for (let k in properties) {
      out.push(...formatComponentProp(properties[k], k, optional))

      if (docRef.description) out.push(docRef.description);
    }
    return out;
  }
  
  const ref = allRefs[docRef.type];
  if (ref) {
    return formatComponentProps(ref[0], allRefs, optional);
  }

  return Code(formatType(docRef));
}

const formatComponentProp = (docRef: any, key: string, optional: boolean) => {
  const title = key + '<span className="muted">' + (optional || docRef.optional ? '?' : '') + '</span>';
  const tokens = formatType(docRef);

  const out: string[] = [];
  const short = tokens.join(' ');
  if (short.length < 70 && !short.match(/\n/)) {
    out.push("\n");
    out.push('<div className="inline-row">');
    out.push('<b>' + title + '</b>');
    out.push('<pre className="inline"><code className="language-tsx">' + short.replace(/</, '&lt;') + '</code></pre>');
    out.push('</div>');
    out.push("\n");
    if (docRef.description) out.push(docRef.description);
  }
  else {
    out.push(...B(title));
    out.push(...Code(tokens));
  }
  return out;
}

const indent = (lines: string[]) => {
  const out: string[] = [];
  let indent = 0;
  const dbg = (lines.join(' ').match(/GridProps/));
  for (const line of lines) {
    let last = indent;

    let inc = line.replace(/[^{(]+/g, '').length;
    let dec = line.replace(/[^})]+/g, '').length;

    indent -= dec;
    indent += inc;

    let lead = (inc > dec) || !line.match(/^\s*[})]/);
    out.push('  '.repeat(Math.max(0, lead ? last : indent)) + line);
  }
  return out;
}

const formatTypeAlias = (docRef: any) => {
  const type = formatType(docRef);
  
  let suffix = ''
  if (docRef.typeParams) {
    const types = docRef.typeParams.map((t: any) => formatType(t));
    suffix = '<' + types.join(', ') + '>';
  }

  const short = type.join(" ");
  if (short.length < 70) return [`type ${docRef.id}${suffix} = ${short}`];
  return `type ${docRef.id}${suffix} =\n${type.join("\n")}`.split("\n");
}

const formatFunction = (docRef: any) => {
  const {signatures} = docRef;  
  const sig = signatures
    ? signatures.length === 1
      ? formatSignature(signatures[0])
      : ['interface {', ...signatures.flatMap((s: any) => (formatSignature(s).join("\n") + ';').split("\n")), '}']
    : [];
  return `const ${docRef.id}: ${sig.join("\n")}`.split("\n");
}

const formatConstant = (docRef: any, value: any = '') => {
  const type = formatType(docRef);
  let short = type.join(' ');

  const prefix = `const ${docRef.id}:`;
  const suffix = value ? (` = ` + formatType(value).join("\n")).split("\n") : [];
  const tail = suffix.join(' ');

  return (short.length + tail.length < 70) ? [prefix +' '+ short + suffix.join(' ')] : [prefix, ...type, ...suffix];
}

const LITERAL_TYPES = new Set(['number']);

const formatType = (docRef: any) => {

  if (LITERAL_TYPES.has(docRef.type)) {
    return [docRef.type];
  }
  
  if (docRef.type === 'typeparam') {
    let type = docRef.name;
    let suffix = docRef.default ? ' = ' + docRef.default : '';
    if (docRef.implements) {
      type += ' extends ' + formatType(docRef.implements[0]).join(' ');
    }
    return [type + suffix];
  }

  if (docRef.type === 'union') {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));

    const short = types.map((t: any) => t.join(' ')).join(" | ");  
    if (short.length < 70) return [short];
    return ['', ...types.map((t: any) => '| ' + t.join(' '))];
  }

  if (docRef.type === 'intersection') {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));

    const short = types.map((t: any) => t.join(' ')).join(" & ");
    if (short.length < 70) return [short];

    const inter = types.map((t: any) => t.join('\n')).join(' &\n');
    return inter.replaceAll(' &\n{}', '').split('\n');
  }

  if (docRef.type === 'tuple') {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));

    const short = '[' + types.map((t: any) => t.join(' ')).join(", ") + ']';
    if (short.length < 70) return [short];
    return ['[', ...types.join(',\n').split('\n'), ']'];
  }
  
  if (docRef.type === 'interface') {
    const {signatures} = docRef;
    const out: string[] = ['interface ' + docRef.id + ' {'];
    if (signatures) {
      for (const sig of signatures) {
        out.push(...formatSignature(sig));
      }
      out.push('}');
    }
    else {
      out[0] += '}';
    }
    return out;
  }
  
  if (docRef.type === 'enum') {
    const {properties} = docRef;
    const out: string[] = [];
    for (let k in properties) {
      const child = formatChild(properties[k]);
      out.push(...child);
    }
    return ["enum {", ...out, "}"];
  }

  if (docRef.type === 'Function') {
    const {signatures} = docRef;
    const out: string[] = [];
    if (signatures) {
      for (const sig of signatures) {
        out.push(...formatSignature(sig));
      }
      return out;
    }
  }
  
  if (docRef.type === 'Object') {
    const {properties} = docRef;
    const out: string[] = [];
    for (let k in properties) {
      const child = formatChild(properties[k]);
      out.push(...child);
    }
    return ["{", ...out, "}"];
  }

  if (docRef.type === 'mapped') {
    const {typeLiteral} = docRef;
    return typeLiteral.split('\n');
    /*
    const types = typeArgs.map((t: any) => formatType(t));

    const select = docRef.key.name + ' in ' + docRef.key.implements.flatMap(formatType).join(' ');
    const short = '{[' + select + ']: ' + types.join(' ') + '}';
    if (short.length < 70) return [short];

    return ['{', '[' + select + ']: ' + types.join('\n'), '}'].join('\n').split('\n');
    */
  }
  
  if (docRef.type === 'indexed') {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));

    const short = types[0].join(' ') + '[' + types[1].join(' ') + ']';
    return [short];
  }

  if (docRef.type === 'conditional') {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));

    const out = [...types[0], 'extends', ...types[1], '?', ...types[2], ':', ...types[3]];
    const short = out.join(' ');
    if (short.length < 70) return [short];
    return out;
  }
  
  const typeString = docRef.type;
  if (docRef.typeArgs) {
    const {typeArgs} = docRef;
    const types = typeArgs.map((t: any) => formatType(t));
    const tokens = types.map((t: any) => t.join(' '));

    if (docRef.type === 'Array') {
      const short = autoParen(tokens.join(', ')) + '[]';
      if (short.length < 70) return [short];
      return (autoParen(types.flat().join('\n')) + '[]').split('\n');
    }

    const short = typeString + '<' + tokens.join(', ') + '>';
    if (short.length < 70) return [short];
    return (typeString + '<' + tokens.join(',\n') + '>').split('\n');
  }

  return [typeString];
}

const formatChild = (docRef: any) => {
  const out: string[] = [];
  
  if (docRef.kind === 'property') {
    out.push(...formatProperty(docRef));
  }
  else if (docRef.kind === 'parameter') {
    out.push(...formatProperty(docRef));
  }
  else if (docRef.kind === 'enummember') {
    const prop = docRef.id.split(/[\^.]/g).pop();
    out.push(prop);
  }
  else {
    console.log('!!! unstyled child', {docRef})
    out.push(`${docRef.id}`);
  }

  return out;
};

const formatProperty = (docRef: any) => {
  const out: string[] = [];
  const {id, name, type, optional, rest} = docRef;
  const prop = name ?? id.split(/[\^.]/g).pop();

  const prefix = rest ? '...' : '';

  if (optional) out.push(prefix + prop + '?:');
  else out.push(prefix + prop + ':');
  
  out.push(formatType(docRef).join("\n") + ',');
  
  const short = out.join(' ');
  if (short.length < 70) return [short];
  return out;
}

const formatSignature = (signature: any, tail: string = ' =>') => {
  const {typeParams, params, returns} = signature;

  let prefix = '';
  if (typeParams?.length) {
    prefix = '<' + typeParams.map(formatType).join(', ') + '>';
  }

  const out: string[] = [];
  if (params?.length) {
    out.push(prefix + '(');
    out.push(...params.map(formatChild).map((s: any) => s.join(" ")));
    out.push(')');
  }
  else {
    out.push(prefix + '()');
  }
  const ret = formatType(returns ?? {type: 'void'});
  if (ret.join(" ").length < 70) {
    out[out.length - 1] += tail + ' '+ ret.join(" ");
  }
  else {
    out[out.length - 1] += tail + ' ' + ret.join("\n");
  }

  const short = out.join(" ");
  if (short.length < 40) {
    return [short];
  }
  return out.join("\n").split("\n");
}

const autoParen = (s: string) => {
  if (s.match(/ /)) {
    return '(' + s + ')';
  }
  return s;
}
