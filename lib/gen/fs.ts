import { readFileSync } from 'fs';
import glob from 'glob';
import { join } from 'path';
import matter from 'gray-matter';
import { MetaFile } from '../types';

const markdownDirectory = join(process.cwd(), './docs');
const packagesDirectory = join(process.cwd(), './docs/packages');

export const getAllPackages = () => {
  const files = glob.sync(join(packagesDirectory, '*.json'));
  return files.map(file => {
    const json = readFileSync(file, 'utf-8');
    try {
      const meta = JSON.parse(json);
      if (!meta.name.match(/@use-gpu/)) return null;

      let content = '';
      const readme = file.replace('.json', '.readme');
      try {
        content = readFileSync(readme).toString();
      } catch (e) {};

      return {
        file,
        meta,
        content,
      } as MetaFile;
    } catch (e) {
      return null;
    }
  }).filter(x => !!x) as MetaFile[];
};

export const getAllMarkdown = () => {
  const files = glob.sync(join(markdownDirectory, '**/*.md'));
  return files.map(file => {
    const md = readFileSync(file, 'utf-8');
    const { data, content } = matter(md);
    return {
      file,
      meta: data,
      content,
    } as MetaFile;
  }).filter(x => !!x) as MetaFile[];
};

export const getAllTSDocs = () => {
  const DOCS_JSON = './docs/ts-docs.json';

  try {
    const json = readFileSync(DOCS_JSON, 'utf-8');
    return JSON.parse(json);
  } catch (e) {};

  return {};
}

export const getPackageVersion = () => {
  const PACKAGE_JSON = './docs/packages/core.json';

  try {
    const json = readFileSync(PACKAGE_JSON, 'utf-8');
    const meta = JSON.parse(json);
    return meta?.version;
  } catch (e) {};

  return 'x.x.x';
}
