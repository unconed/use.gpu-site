## Use.GPU Docs Website

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Run the development server:

```sh
yarn install
yarn dev
```

Regenerate doc index / reparse markdown:

```sh
yarn gen
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Generating Docs

Markdown docs live in `/docs/*.md` and are loaded into a tree outline by folder structure.

Typescript docs live in `/docs/ts-docs.json`. The latest snapshot is checked in here, so as to simplify builds.

The script to generate it lives in the `use-gpu` repo, which must be checked out beside this one (i.e. at `../use.gpu`). Run `yarn build` in `use.gpu` to generate `ts-docs.json`.

After edits are made, you have to regenerate the doc index by restarting the dev server or running `yarn gen`.



