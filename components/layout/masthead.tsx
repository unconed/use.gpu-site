import React, { FC } from 'react';
import { styled } from '../../lib/stitches.config';
import Link from 'next/link';

import { STYLES, FONTS, COLORS, GRID } from './constants';

export const Panel = styled('div', {
  width: '100%',
  height: '100%',
  paddingRight: 64,
  'body.animated &': {
    transition: 'padding-right 0.25s ease-in-out',
  },
  "@media (max-width: 768px)": {
    paddingLeft: 32,
    paddingRight: 48,
  },
  "@media (min-width: 1260px)": {
    paddingRight: 116,
  },
  "@media (min-width: 1440px)": {
    paddingRight: 176,
  },
  display: 'flex',
  alignItems: 'center',
});

export const Title = styled('div', {
  ...FONTS.headerPage,
  fontFamily: "Lato",
  fontWeight: 900,
});

export const Span = styled('span', {
  color: COLORS.textBrand,
});

export const Grow = styled('div', {
  flexGrow: 1,
});

export const Left = styled('div', {
  ...STYLES.paddedLeftRight,
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingBottom: '3px',
});

export const Right = styled('div', {
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingBottom: '3px',
  flexGrow: 1,
});

export const Version = styled('span', {
  marginLeft: GRID.s,
  opacity: 0.5,
  color: COLORS.textBody,
  paddingTop: '2px',
  whiteSpace: 'nowrap',
});

export const NavLink = styled('a', {
  background: COLORS.bgButton,
  color: COLORS.textActive,
  padding: `${GRID.s}px ${GRID.m}px`,
  borderRadius: '3px',
  textAlign: 'center',
  fontSize: '0.9em',
  width: '120px',
  "@media (max-width: 768px)": {
    width: 'auto',
    padding: `${GRID.s}px ${GRID.s * 1.5}px`,
  },
  '&:hover': {
    background: COLORS.bgButtonHover,
  },
  variants: {
    main: {
      true: {
        fontWeight: 700,
        background: COLORS.bgPrimary,
        '&:hover': {
          background: COLORS.bgPrimaryHover,
        },
      },
    },
    right: {
      true: {
        marginLeft: GRID.s,
      }
    }
  }
});

export const HideLong = styled('div', {
  "@media (max-width: 768px)": {
    "& > span": {
      display: 'none',
    }
  },
});

type Props = {
  version?: string,
};

export const Masthead: FC<Props> = (props: Props) => {
  const {version} = props;
  return (<Panel>
    <Left>
      <Link href="/">
        <a><Title><Span>Use.</Span>GPU</Title></a>
      </Link>
      <Version> – {version}</Version>
    </Left>
    <Right>
      <Grow />
      <NavLink href="https://gitlab.com/unconed/use.gpu-site" target="_blank" right >
        <HideLong><span>Docs </span>Source</HideLong>
      </NavLink>
      <NavLink href="https://gitlab.com/unconed/use.gpu" target="_blank" right main>
        <HideLong>Git<span> Repo</span></HideLong>
      </NavLink>
    </Right>
  </Panel>)
};