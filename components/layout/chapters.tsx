import React, { useEffect, useState } from 'react';
import { styled } from '../../lib/stitches.config';
import { GRID, COLORS } from './constants';

type Props = {
  chapters: [string, number][],
}

const Panel = styled('div', {
  position: 'sticky',
  top: 0,
  paddingLeft: GRID.s,
  paddingRight: GRID.s,
  paddingBottom: GRID.m,
  paddingTop: GRID.l + GRID.s,
});

const StyledResultGroup = styled('div', {
  paddingLeft: GRID.m,
  paddingTop: GRID.s,
  paddingBottom: GRID.m,
  cursor: 'default',
  userSelect: 'none',
  fontSize: '0.8em',
  fontWeight: 700,
  textTransform: 'uppercase',
});

const StyledChapterRow = styled('div', {
  paddingTop: GRID.s / 4 + 2,
  paddingRight: GRID.s,
  paddingBottom: GRID.s / 4 + 2,
  lineHeight: '20px',
  cursor: 'default',
  userSelect: 'none',
  fontSize: '0.85em',
  '&:hover': {
    background: COLORS.bgHover,
  },
  variants: {
    active: {
      true: {
        fontWeight: 'bold',
        color: COLORS.textActive,
      },
    },
  },
});

const SCROLL_OFFSET = 200;
const SCROLL_START = 50;

export const Chapters = (props: Props) => {
  const {chapters} = props;
  const [read, setRead] = useState<number>(0);

  const makeScrollTo = (index: number) => () => {
    const hs = document.querySelectorAll('.markdown-content h1, .markdown-content h2, .markdown-content h3, .markdown-content h4');
    const h = hs[index + 1];
    const rect = h.getBoundingClientRect();
    const scrollY = (window.scrollY || document.body.scrollTop) + rect.top - SCROLL_OFFSET + SCROLL_START;
    window.scrollTo(0, scrollY);
    document.body.scrollTop = scrollY;
  };

  useEffect(() => {
    const handleScroll = () => {
      const hs = document.querySelectorAll('.markdown-content h1, .markdown-content h2, .markdown-content h3, .markdown-content h4');
      let i = 0;
      for (const h of Array.from(hs).slice(1)) {
        const rect = h.getBoundingClientRect();
        if (rect.top > SCROLL_OFFSET) break;
        ++i;
      }
      setRead(i);
    };

    window.addEventListener('scroll', handleScroll);
    document.body.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
      document.body.removeEventListener('scroll', handleScroll);
    }
  }, [])

  return (
    <Panel>
      <StyledResultGroup>Sections</StyledResultGroup>
      {chapters.slice(1).map(([text, depth], i) => {
        const active = read === i + 1;
        return (<a key={i.toString()}>
          <StyledChapterRow
            active={active}
            style={{paddingLeft: Math.max(1, depth - 1) * GRID.m}}
            onClick={makeScrollTo(i)}
          >
            {active ? text : text + '  '}
          </StyledChapterRow>
        </a>);
      })}
    </Panel>
  );
};
