import React, { FC } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

type Props = {
  title?: string,
}

export const Meta: FC<Props> = ({title}: Props) => {
  const url = 'https://usegpu.live' + useRouter().asPath;
  const text = 'Use.GPU Docs' + (title ? ` - ${title}` : '');
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content="Reactive WebGPU component library" />
      <meta name="og:description" content="Reactive WebGPU component library" />
      <meta name="twitter:description" content="Reactive WebGPU component library" />
      <meta name="viewport" content="width=device-width, minimum-scale=1.0" />

      <meta name="author" content="Steven Wittens" />

      <meta name="og:image" content="https://usegpu.live/cover.jpg" />
      <meta name="twitter:image" content="https://usegpu.live/cover.jpg" />

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@unconed" />
      <meta name="twitter:creator" content="@unconed" />
      <meta name="twitter:title" content={text} />

      <meta name="og:title" content={text} />
      <meta name="og:site_name" content="usegpu.live" />
      <meta name="og:url" content={url} />
    
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
}