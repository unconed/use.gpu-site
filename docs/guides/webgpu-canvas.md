---
title: WebGPU canvas
order: 1
---

# WebGPU canvas

## Overview

To use WebGPU, you need to create a `<canvas>`. This is done with `@{<WebGPU>}` and `@{<AutoCanvas>}`:
  
```tsx
import { WebGPU, AutoCanvas } from '@use-gpu/webgpu';

const App = () => {
  return (
    <WebGPU>
      <AutoCanvas
        selector="#use-gpu .canvas"
        samples={4}
      >

      </AutoCanvas>
    </WebGPU>
  )
};
```

- `@{<WebGPU>}` will mount a native `GPUDevice` and provide it via a `@{DeviceContext}`.
- `@{<AutoCanvas>}` will create or adopt an HTML `<canvas>` to render to, and size-to-fit inside its container.

The CSS `selector` determines the behavior. If it points to a `<canvas>`, it is adopted and used as is. Otherwise, a new `<canvas>` is created inside.

If you want to control the canvas more directly, you can use `@{<Canvas>}`.

## Fallback content

If WebGPU is not available, `@{<WebGPU>}` will render its `fallback` prop with an error. This must be other Live components.
  
To render an HTML message using React, use the `@{<HTML>}` portal from `@use-gpu/react`:

```tsx
import React from '@use-gpu/live';
import { HTML } from '@use-gpu/react';

import { makeErrorMessage } from './ErrorMessage';

// Live component
const App = () => {

  const root = document.querySelector('#use-gpu');

  return (
    // Live-flavored JSX
    <WebGPU
      fallback={(error: Error) =>
        <HTML container={root}>
          {makeErrorMessage(error)}
        </HTML>
    >
    </WebGPU>
  );
}
```

```tsx
import React from 'react';

// React component
export const ErrorMessage = ({error}) => {
  return (
    // React-flavored JSX
    <span>{error.message}</span>
  );
}

export const makeErrorMessage = (error: Error) =>
  <ErrorMessage error={error} />;
```

Note that it is fine to pass React-flavored JSX into Live (but not the other way around). It is cleaner and faster however to avoid mixing them.
