---
title: Background
order: 2
---

# Background

<img src="/images/inspect.png" alt="Live inspector" style="width: 800px; max-width: 100%;">

These talks and articles discuss the background of **Use.GPU** and the architecture of the **Live** effect run-time.

**LambdaConf 2024**

- Why is GPU programming still such a niche?
- Why do even seasoned programmers struggle with it?
- Why is it a bad proposition for businesses, in-house or 3rd party?
- How does reactive and declarative programming fit into this?

<div style="position: relative; width: 100%; padding-bottom: 56%; margin: 48px 0;">
  <iframe frameborder="0" allowfullscreen="allowfullscreen" style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; width: 100%; height: 100%; border: 0;" src="https://www.youtube.com/embed/TAQ7yBLRZ3U"></iframe>
</div>

### Use.GPU

- [The Case for Use.GPU](https://acko.net/blog/the-case-for-use-gpu/)<br>Why GPU programming is so different, why existing approaches fall short, and how to fix it.

- [Frickin' Shaders with Frickin' Laser Beams](https://acko.net/blog/frickin-shaders-with-frickin-laser-beams/)<br>On shader closures and the Use.GPU shader linker and tree shaker.

- [On Variance and Extensibility](https://acko.net/blog/on-variance-and-extensibility/)<br>
  How to design extensible, composable systems.

### Live

These articles explain Effect-based programming and how to combine it with a data-flow and memoization perspective.

 - [Climbing Mount Effect](https://acko.net/blog/climbing-mt-effect/) - Effect-based programming
 - [Reconcile All The Things](https://acko.net/blog/reconcile-all-the-things/) - Memoization and reconciliation
 - [Live - Headless React](https://acko.net/blog/live-headless-react/) - Live run-time and WebGPU
