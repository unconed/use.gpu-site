import { basename, join } from 'path';
import { statSync, writeFileSync } from 'fs';
import { buildDocumentTree } from '../lib/gen/outline';

console.log("Generating ts-docs / markdown index...");

const SRC_DOCS = join(basename(__filename), "../docs/ts-docs.json");
try {
  statSync(SRC_DOCS);
} catch (e) {
  console.error(SRC_DOCS + ' not found. Run `yarn build` in adjacent `use-gpu` repo checkout.');
  process.exit(1);
}

const TARGET_INDEX = join(basename(__filename), "../docs/index.json");
const TARGET_SLUGS = join(basename(__filename), "../docs/slugs.json");

(async () => {
  const {slugs, nodes, indexed} = await buildDocumentTree();
  writeFileSync(TARGET_INDEX, JSON.stringify({nodes, indexed}, null, 2));
  writeFileSync(TARGET_SLUGS, JSON.stringify(slugs, null, 2));
})();
